from os import *

from flask import Flask, render_template, request, flash, session, url_for, redirect
from controller.UserLoginController import UserLoginController
from controller.UserProfileController import UserProfileController
from controller.AccountController import CreateAccountController
from controller.AdminCreateUserAccController import AdminCreateUserAccController
from controller.AdminViewUserAccController import AdminViewUserAccController
from controller.AdminSearchUserAccController import AdminSearchUserAccController
from controller.AdminEditUserAccController import AdminEditUserAccController
from controller.AdminCreateUserProfController import AdminCreateUserProfController
from controller.AdminViewUserProfController import AdminViewUserProfController
from controller.AdminSearchUserProfController import AdminSearchUserProfController
from controller.AdminEditUserProfController import AdminEditUserProfController
from controller.RestManagerCreateCouponController import RestManagerCreateCouponController
from controller.RestManagerViewCouponController import RestManagerViewCouponController
from controller.RestManagerSearchCouponController import RestManagerSearchCouponController
from controller.RestManagerEditCouponController import RestManagerEditCouponController
from controller.RestManagerDeleteCouponController import RestManagerDeleteCouponController
from controller.RestManagerViewCategoryController import RestManagerViewCategoryController
from controller.RestManagerSearchCategoryController import RestManagerSearchCategoryController
from controller.RestManagerCreateCategoryController import RestManagerCreateCategoryController
from controller.RestManagerDeleteCategoryController import RestManagerDeleteCategoryController
from controller.RestManagerEditCategoryController import RestManagerEditCategoryController
from controller.RestManagerViewMenuItemController import RestManagerViewMenuItemController
from controller.RestManagerSearchMenuItemController import RestManagerSearchMenuItemController
from controller.RestManagerCreateMenuItemController import RestManagerCreateMenuItemController
from controller.RestManagerDeleteMenuItemController import RestManagerDeleteMenuItemController
from controller.RestManagerEditMenuItemController import RestManagerEditMenuItemController

app = Flask(__name__, template_folder="boundary")

SECRET_KEY = b'D\x91\x8c\x94\xc7\xd2F\xce\x96`\x85\xac\xe0\xb0\xe5\xb8\x8c\xb5/\xa0\x1a\xbf\xb0q5Pcw\xe3(\x9b^'
app.config['SECRET_KEY'] = SECRET_KEY

login_interface = UserLoginController()
profile = UserProfileController()
account = CreateAccountController()


def checkNull(*args, **kwargs):
    for i in args:
        if not bool(i and i.strip()):
            return True
    return False


@app.route("/")
def index():
    return render_template('index.html', title='home')


@app.route('/login', methods=['GET', 'POST'])
def displayLoginPage():
    msg, error_msg, is_invalid = "", "", ""

    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:

        if checkNull(request.form['username'], request.form['password']):
            error_msg = 'please enter something'
            is_invalid = 'is-invalid'
            flash('Please fill in the EmployeeID / Password')
            msg = 'Please enter valid EmployeeID /Password'

        else:
            username = request.form['username']
            password = request.form['password']

            print(username, password)
            role = login_interface.loginVaildate(username, password)
            print(role)

            if not role:
                msg = "Please enter valid EmployeeID /Password"
                flash('Please enter valid EmployeeID /Password')



            
            elif role[0][0] == 'owner':
                session['loggedin'] = role[0][0]

                return redirect(url_for('admin_page'))
            elif role[0][0] == 'manager':
                session['loggedin'] = role[0][0]

                return redirect(url_for('manager_page'))
            

    return render_template('login.html', title='login', msg=msg, error_msg=error_msg, is_invalid=is_invalid)

# MANAGER PAGE
@app.route('/manager')
def manager_page():
    if 'loggedin' not in session:
        flash('You need to Login first')
        return redirect(url_for('displayLoginPage'))

    return render_template('sprint2_manager_mainPage.html', title='manager')

@app.route('/menuitem/menuitem_create', methods=['GET', 'POST'])
def displayCreateMenuItemPage():
    msg, error_msg, is_invalid, flash, success_msg = "", "", "", "", ""

    category = RestManagerViewCategoryController.displayCategory()

    if request.method == 'POST':  # form validation
        if 'ITEM NAME' in request.form and 'ITEM DESCRIPTION' in request.form \
                and 'CATEGORY' in request.form and 'PRICE' in request.form:
            # print(*list(request.form.to_dict().values()))

            print('chun was here!')
            if checkNull(*list(request.form.to_dict().values())):
                print('chun was here too!')
                error_msg = 'please enter something'
                is_invalid = 'is-invalid'
                msg = 'Please full up all Menu Item particulars before submitting'
                flash = 'danger'

            else:
                itemName = request.form['ITEM NAME']
                itemDescription = request.form['ITEM DESCRIPTION']
                category = request.form['CATEGORY']
                price = request.form['PRICE']
                print(type(request.form.get('ITEM NAME')))

                if RestManagerCreateMenuItemController.addMenuItem(itemName, itemDescription, category, price):
                    msg = 'menu item successfully created'
                    flash = 'success'
                else:
                    msg = 'there was an error in creating a menu item'
                    flash = 'danger'

    return render_template('create_menuitem.html', title='create_menuitem', msg=msg,
                           error_msg=error_msg, is_invalid=is_invalid, success_msg=success_msg, flash=flash,
                           category=category)

@app.route('/menuitem/menuitem_view', methods=['GET', 'POST'])
def displayViewMenuItemPage():
    result = RestManagerViewMenuItemController.displayMenuItem()
    search = None
    noresult = None
    employeeID = None

    if request.method == 'GET':
        print('chun was here!')
        print(request.args.get('mySearch'))
        employeeID = onSubmit(request.args.get('mySearch'))

        search = RestManagerSearchMenuItemController.displayMenuItemInfo(request.args.get('mySearch'))

        if not search:
            noresult = None

    return render_template('view_menu_item.html', result=result, search=search, noresult=noresult)

@app.route('/menuitem/<string:itemName>/menuitem_edit', methods=['GET', 'POST'])
def displayEditMenuItemPage(itemName):
    search = RestManagerEditMenuItemController.retrieveSearchMenuItemInfo(itemName)
    userData = RestManagerEditMenuItemController.retrieveMenuItemInfo(itemName)
    userRole = RestManagerEditMenuItemController.retrieveCategory(itemName)

    #if not search:
    #    abort(404)

    if request.method == 'POST':
        print(request.form)
        itemName = request.form.get('ITEM NAME')
        itemDescription = request.form.get('ITEM DESCRIPTION')
        category = request.form.get('CATEGORY')
        price = request.form.get('PRICE')
        test = RestManagerEditMenuItemController.updateMenuItem(itemName, itemDescription, category, price)
        return redirect(url_for('displayViewMenuItemPage', test=test))

    print("hiii")
    return render_template('edit_menuitem.html', search=search, itemName=itemName, userData=userData, userRole=userRole)

@app.route('/menuitem/<string:itemName>/menuitem_delete', methods=['GET', 'POST'])
def deleteMenuItemPage(itemName):
    print(itemName)
    print("hello")
    test = RestManagerDeleteMenuItemController.deleteMenuItem(itemName)
    return redirect(url_for('displayViewMenuItemPage'))

@app.route('/category/category_create', methods=['GET', 'POST'])
def displayCreateCategoryPage():
    msg, error_msg, is_invalid, flash, success_msg = "", "", "", "", ""

    if request.method == 'POST':  # form validation
        if 'CATEGORY NAME' in request.form:
            # print(*list(request.form.to_dict().values()))

            print('chun was here!')
            if checkNull(*list(request.form.to_dict().values())):
                print('chun was here too!')
                error_msg = 'please enter something'
                is_invalid = 'is-invalid'
                msg = 'Please fill up all Category particulars before submitting'
                flash = 'danger'

            else:
                categoryName = request.form['CATEGORY NAME']
                print(type(request.form.get('CATEGORY NAME')))

                if RestManagerCreateCategoryController.addCategory(categoryName):
                    msg = 'category successfully created'
                    flash = 'success'
                else:
                    msg = 'there was an error in creating a category'
                    flash = 'danger'

    return render_template('create_category.html', title='create_category', msg=msg,
                           error_msg=error_msg, is_invalid=is_invalid, success_msg=success_msg, flash=flash)

@app.route('/category/category_view', methods=['GET', 'POST'])
def displayViewCategoryPage():
    result = RestManagerViewCategoryController.displayCategory()
    search = None
    noresult = None
    employeeID = None

    if request.method == 'GET':
        print('chun was here!')
        print(request.args.get('mySearch'))
        employeeID = onSubmit(request.args.get('mySearch'))

        search = RestManagerSearchCategoryController.displayCategoryInfo(request.args.get('mySearch'))

        if not search:
            noresult = None

    return render_template('view_category.html', result=result, search=search, noresult=noresult)

@app.route('/category/<string:categoryName>/category_edit', methods=['GET', 'POST'])
def displayEditCategoryPage(categoryName):
    search = RestManagerEditCategoryController.retrieveSearchCategoryInfo(categoryName)
    userData = RestManagerEditCategoryController.retrieveCategoryInfo(categoryName)

    #if not search:
    #    abort(404)

    if request.method == 'POST':
        print(request.form)
        newCategoryName = request.form.get('CATEGORY NAME')
        test = RestManagerEditCategoryController.editCategory(categoryName, newCategoryName)
        return redirect(url_for('displayViewCategoryPage', test=test))

    print("hiii")
    return render_template('edit_category.html', search=search, categoryName=categoryName, userData=userData)

@app.route('/category/<string:categoryName>/category_delete', methods=['GET', 'POST'])
def deleteCategoryPage(categoryName):
    print(categoryName)
    print("hello")
    test = RestManagerDeleteCategoryController.deleteCategory(categoryName)
    return redirect(url_for('displayViewCategoryPage'))

@app.route('/coupon/coupon_create', methods=['GET', 'POST'])
def displayCreateCouponPage():
    msg, error_msg, is_invalid, flash, success_msg = "", "", "", "", ""

    if request.method == 'POST':  # form validation
        if 'COUPON NAME' in request.form and 'DISCOUNT AMOUNT' in request.form:
            print(*list(request.form.to_dict().values()))

            print('chun was here!')
            if checkNull(*list(request.form.to_dict().values())):
                print('chun was here too!')
                error_msg = 'please enter something'
                is_invalid = 'is-invalid'
                msg = 'Please fill up all Coupon particulars before submitting'
                flash = 'danger'

            else:
                couponCode = request.form['COUPON NAME']
                discount = request.form['DISCOUNT AMOUNT']
                print(type(request.form.get('COUPON NAME')))

                if RestManagerCreateCouponController.createCoupon(couponCode, discount):
                    msg = 'coupon successfully created'
                    flash = 'success'
                else:
                    msg = 'there was an error in creating a coupon'
                    flash = 'danger'

    return render_template('create_coupon.html', title='create_coupon', msg=msg,
                           error_msg=error_msg, is_invalid=is_invalid, success_msg=success_msg, flash=flash)

@app.route('/coupon/coupon_view', methods=['GET', 'POST'])
def displayViewCouponPage():
    result = RestManagerViewCouponController.displayCoupon()
    search = None
    noresult = None
    employeeID = None

    if request.method == 'GET':
        print('chun was here!')
        print(request.args.get('mySearch'))
        employeeID = onSubmit(request.args.get('mySearch'))

        search = RestManagerSearchCouponController.displayCouponInfo(request.args.get('mySearch'))

        if not search:
            noresult = None

    return render_template('view_coupon.html', result=result, search=search, noresult=noresult)

@app.route('/coupon/<string:couponCode>/coupon_edit', methods=['GET', 'POST'])
def displayEditCouponPage(couponCode):
    search = RestManagerEditCouponController.retrieveSearchCouponInfo(couponCode)
    userData = RestManagerEditCouponController.retrieveCouponInfo(couponCode)

    #if not search:
    #    abort(404)

    if request.method == 'POST':
        print(request.form)
        couponCode = request.form.get('COUPON NAME')
        discount = request.form.get('DISCOUNT AMOUNT')
        test = RestManagerEditCouponController.editCoupon(couponCode, discount)
        return redirect(url_for('displayViewCouponPage', test=test))

    print("hiii")
    return render_template('edit_coupon.html', search=search, couponCode=couponCode, userData=userData)

@app.route('/coupon/<string:couponCode>/coupon_delete', methods=['GET', 'POST'])
def deleteCouponPage(couponCode):
    print(couponCode)
    print("hello")
    test = RestManagerDeleteCouponController.deleteCoupon(couponCode)
    return redirect(url_for('displayViewCouponPage'))

# ADMIN PAGE
@app.route('/admin')
def admin_page():
    if 'loggedin' not in session:
        flash('You need to Login first')
        return redirect(url_for('displayLoginPage'))

    return render_template('admin.html', title='admin')


# LOGOUT
@app.route('/logout')
def logout_page():
    session.pop('loggedin', None)  # deletes the session
    return redirect(url_for('displayLoginPage'))


# CREATE ACCOUNT
@app.route('/accounts/account_create', methods=['GET', 'POST'])
def displayCreateUserAccPage():
    msg, error_msg, is_invalid, flash, success_msg = "", "", "", "", ""

    user_profile = profile.retrieveUserProfInfo()

    if request.method == 'POST':  # form validation
        if 'FULL NAME' in request.form and 'ADDRESS' in request.form \
                and 'BIRTHDAY' in request.form and 'EMPLOYEEID' in request.form \
                and 'PASSWORD' in request.form:
            print(*list(request.form.to_dict().values()))

            print('chun was here!')
            if checkNull(*list(request.form.to_dict().values())):
                print('chun was here too!')
                error_msg = 'please enter something'
                is_invalid = 'is-invalid'
                msg = 'Please full up all Account particulars before submitting'
                flash = 'danger'

            else:
                full_name = request.form['FULL NAME']
                address = request.form['ADDRESS']
                nric_fin = request.form['NRIC_FIN']
                bday = request.form['BIRTHDAY']
                employeeID = request.form['EMPLOYEEID']
                password = request.form['PASSWORD']
                role = request.form['ROLES']
                print(type(request.form.get('ROLES')))

                if AdminCreateUserAccController.addUserAcc(full_name, address, nric_fin, bday, employeeID, password, role):
                    msg = 'account successfully created'
                    flash = 'success'
                else:
                    msg = 'there was an error in creating an account'
                    flash = 'danger'

    return render_template('account_create.html', title='create_account', msg=msg,
                           error_msg=error_msg, is_invalid=is_invalid, success_msg=success_msg, flash=flash,
                           user_profile=user_profile)


def onSubmit(employeeID):
    if isinstance(employeeID, int):
        return employeeID
    return None


# VIEW ACCOUNT
@app.route('/account/view_account', methods=['GET', 'POST'])
def displayViewUserAccPage():
    result = AdminViewUserAccController.displayUserAcc()
    search = None
    noresult = None
    employeeID = None

    if request.method == 'GET':
        print('chun was here!')
        print(request.args.get('mySearch'))
        employeeID = onSubmit(request.args.get('mySearch'))

        search = AdminSearchUserAccController.displayUserAccInfo(request.args.get('mySearch'))

        if not search:
            noresult = None

    return render_template('view_account.html', result=result, search=search, noresult=noresult)

# EDIT USER
@app.route('/account/<int:EmployeeID>/edit_account', methods=['GET', 'POST'])
def displayEditUserAccount(EmployeeID):
    search = AdminEditUserAccController.retrieveSearchUserAccInfo(int(EmployeeID))
    userData = AdminEditUserAccController.retrieveUserAccInfo(int(EmployeeID))
    userRole = AdminEditUserAccController.retrieveUserProf(int(EmployeeID))

    #if not search:
    #    abort(404)

    if request.method == 'POST':
        print(request.form)
        empID = request.form.get('EMPLOYEEID')
        pwd = request.form.get('PASSWORD')
        fn = request.form.get('FULLNAME')
        address = request.form.get('ADDRESS')
        nric = request.form.get('NRIC')
        bday = request.form.get('BIRTHDAY')
        role = request.form.get('ROLES')
        is_active = request.form.get('r1')
        # test = AdminEditUserAccController.editUserAcc(fn, address, nric, bday, empID, pwd, role, is_active)
        return redirect(url_for('displayViewUserAccPage', test=test))

    return render_template('edit_account.html', search=search, EmployeeID=EmployeeID, userData=userData, userRole=userRole)

# CREATE PROFILE
@app.route('/accounts/create_profile', methods=['GET', 'POST'])
def displayCreateUserProfPage():
    create_profile, msg = None, None

    manage_user_acc = 1 if request.form.get('p1') else 0
    manage_user_pro = 1 if request.form.get('p2') else 0
    manage_menu_item = 1 if request.form.get('p3') else 0
    manage_menu_cat = 1 if request.form.get('p4') else 0
    manage_coupon_codes = 1 if request.form.get('p5') else 0
    manage_orders = 1 if request.form.get('p6') else 0
    manage_report = 1 if request.form.get('p7') else 0

    # privileges = **request.form

    if request.method == 'POST':
        roles = request.form['roles'] or None
        # print('chun was here')
        create_profile = AdminCreateUserProfController.addUserProf(roles, manage_user_acc, manage_user_pro, manage_menu_item,
                                                                   manage_menu_cat, manage_coupon_codes, manage_orders,
                                                                   manage_report)

        if create_profile:
            msg = 'successfully created profile'
        else:
            msg = 'error creating profile'

    return render_template('account_create_profile.html', title='view_profile', msg=msg)


# VIEW PROFILE
@app.route('/accounts/view_profile', methods=['GET'])
def displayViewUserProfPage():
    result = AdminViewUserProfController.displayUserProf()
    search = None
    noresult = None
    search_result = None

    if request.method == 'GET':
        print(request.args.get('mySearch'))
        search_result = onSubmit(request.args.get('mySearch'))
        
        search = AdminSearchUserProfController.displayUserProfInfo(request.args.get('mySearch'))
        if not search:
            noresult = None

    return render_template('view_profile.html', result=result,search=search, noresult=noresult)

@app.route('/accounts/<string:roles>/edit_profile', methods=['GET', 'POST'])
def displayEditUserProfile(roles):
    search = AdminEditUserProfController.retrieveSearchUserProfInfo(roles)
    userData = AdminEditUserProfController.retrieveUserProfInfo()
    #if not search:
    #    abort(404)

    if request.method == 'POST':
        print(request.form)
        roles = request.form.get('ROLES')
        manage_user_acc = 1 if request.form.get('p1') else 0
        manage_user_pro = 1 if request.form.get('p2') else 0
        manage_menu_item = 1 if request.form.get('p3') else 0
        manage_menu_cat = 1 if request.form.get('p4') else 0
        manage_coupon_codes = 1 if request.form.get('p5') else 0
        manage_orders = 1 if request.form.get('p6') else 0
        manage_report = 1 if request.form.get('p7') else 0
        test = AdminEditUserProfController.editUserProf(roles, manage_user_acc, manage_user_pro, manage_menu_item,
                                                manage_menu_cat,
                                                manage_coupon_codes, manage_orders,
                                                manage_report)
        return redirect(url_for('displayViewUserProfPage', test=test))

    return render_template('edit_profile.html', search=search, roles = roles, userData=userData)


@app.cli.command('test')
def test():
    import unittest
    test = unittest.TestLoader().discover('test_sprint_1')
    unittest.TextTestRunner(verbosity=2).run(test)





if __name__ == "__main__":
    app.run(debug=True, use_debugger=False, use_reloader=False)
