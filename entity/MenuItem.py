import entity.SQL as SQL


class MenuItem:
    def __init__(self):
        pass

    @staticmethod
    def addMenuItem(itemName, itemDescription, category, price):
        add_menu_item = f'INSERT INTO menuitem (itemName, description, category, price) VALUES ("{itemName}","{itemDescription}","{category}","{price}")'
        print(add_menu_item)
        query = SQL.sql_insert(query=add_menu_item)
        print(query)

        return query

    @staticmethod
    def displayMenuItem() -> []:
        get_all_menu_item = f'SELECT * FROM menuitem'

        query = SQL.sql_retrieve_all(get_all_menu_item)

        return query

    @staticmethod
    def validateSearch(itemName):
        search_term = f'SELECT itemName FROM menuitem WHERE itemName = "{itemName}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveMenuItemInfo(itemName):
        search_term = f'SELECT * FROM menuitem where itemName = "{itemName}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def updateMenuItem(itemName, itemDescription, category, price):
        search_term = f'UPDATE menuitem SET menuitem.description = "{itemDescription}", menuitem.category = "{category}", menuitem.price = "{price}" WHERE menuitem.itemName = "{itemName}";'
        query = SQL.sql_update(search_term)

        return query

    @staticmethod
    def deleteMenuItem(itemName):
        search_term = f'DELETE FROM menuitem WHERE menuitem.itemName = "{itemName}";'
        query = SQL.sql_update(search_term)

        return query

    @staticmethod
    def retrieveCategory(itemName):
        search_term = f'SELECT category.CategoryName FROM category where category.CategoryName != (SELECT menuitem.category FROM menuitem WHERE menuitem.itemName="{itemName}")'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveSearchMenuItemInfo(itemName):
        search_term = f'SELECT itemName, description, category, price FROM menuitem where itemName = "{itemName}"'
        query = SQL.sql_retrieve_all(search_term)

        return query