import entity.SQL as SQL


class UserAccount:
    def __init__(self):
        pass
    
    @staticmethod
    def loginVaildate(username, password):
        user_detail = SQL.sql_retrieve_all(
            f'SELECT roles FROM useraccount where employeeID="{username}" AND password = "{password}";')
        if not user_detail:
            return False
        
        # user_detail = SQL.sql_retrieve_all(
            # f'SELECT is_active FROM useraccount where employeeID="{username}" AND password = "{password}";')
        # if user_detail == 0:
            # return False
        
        return user_detail

    @staticmethod
    def addUserAcc(full_name, address, nric, birthday, employeeID, password, role):
        # if role=="useradmin":
        add_useradmin = f'INSERT INTO useradmin (employeeID, password) VALUES ("{employeeID}","{password}")'
        print(add_useradmin)
        query = SQL.sql_insert(query=add_useradmin)
        print(query)

        add_user = f'INSERT INTO useraccount (fullname, address, nric_fin, bday, employeeID, password, roles, is_active) VALUES ("{full_name}","{address}","{nric}","{birthday}","{employeeID}","{password}",(select roles from userprofile where roles ="{role}"),1)'
        print(add_user)
        query = SQL.sql_insert(query=add_user)
        print(query)

        return query

    @staticmethod
    def displayUserAcc() -> []:
        get_all_user = f'SELECT * FROM useraccount'

        query = SQL.sql_retrieve_all(get_all_user)

        return query

    @staticmethod
    def validateSearch(employeeID):
        search_term = f'SELECT employeeID FROM useraccount WHERE employeeID="{employeeID}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveUserAccInfo(employeeID):
        search_term = f'SELECT * FROM useraccount where employeeID = "{employeeID}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def updateUserAcc(fullname, address, nric, bday, employeeID, password, role, is_active):
        set_fkcheck_false = f'SET FOREIGN_KEY_CHECKS=0;'
        query = SQL.sql_update(set_fkcheck_false)

        search_term = f'UPDATE useradmin LEFT join useraccount ON useradmin.employeeid = useraccount.employeeid SET useradmin.employeeid = "{employeeID}", useradmin.password = "{password}", useraccount.fullname = "{fullname}", useraccount.address = "{address}" , useraccount.nric_fin = "{nric}", useraccount.bday = "{bday}", useraccount.employeeid ="{employeeID}", useraccount.password = "{password}", useraccount.roles = "{role}", useraccount.is_active = "{is_active}" WHERE useradmin.employeeid = "{employeeID}";'
        query = SQL.sql_update(search_term)
        print("hiiiiiiiiiiiiiiiii" + str(query))

        set_fkcheck_true = f'SET FOREIGN_KEY_CHECKS=1;'
        query = SQL.sql_update(set_fkcheck_true)

        return query
    
    @staticmethod
    def retrieveUserProf(employeeID):
        get_roles = f'SELECT up.roles FROM userprofile up WHERE up.roles != (SELECT ua.roles FROM useraccount ua WHERE ua.employeeid="{employeeID}")'
        
        query = SQL.sql_retrieve_all(get_roles)
        
        return query
    
    @staticmethod
    def retrieveSearchUserAccInfo(employeeID):
        search_term = f'SELECT ua.name, ua.address, ua.nric, ua.bday, ua.employeeID, ua.password, up.roles, ua.is_active FROM useraccount ua INNER JOIN userprofile up ON ua.roles = up.roles WHERE ua.EmployeeID="{employeeID}"'
        
        query = SQL.sql_retrieve_all(search_term)
        
        return query