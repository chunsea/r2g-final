import entity.SQL as SQL


class Coupon:
    def __init__(self):
        pass

    @staticmethod
    def createCoupon(couponCode, discount):
        add_coupon = f'INSERT INTO coupon (CouponCode, DiscountPrice) VALUES ("{couponCode}","{discount}")'
        print(add_coupon)
        query = SQL.sql_insert(query=add_coupon)
        print(query)

        return query

    @staticmethod
    def displayCoupon() -> []:
        get_all_coupon = f'SELECT * FROM coupon'

        query = SQL.sql_retrieve_all(get_all_coupon)

        return query

    @staticmethod
    def validateSearch(couponCode):
        search_term = f'SELECT CouponCode FROM coupon where CouponCode = "{couponCode}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveCouponInfo(couponCode):
        search_term = f'SELECT * FROM coupon where CouponCode = "{couponCode}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveSearchCouponInfo(couponCode):
        search_term = f'SELECT CouponCode, DiscountPrice FROM coupon where CouponCode = "{couponCode}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def updateCoupon(couponCode, discount):
        search_term = f'UPDATE coupon SET coupon.DiscountPrice = "{discount}" WHERE coupon.CouponCode = "{couponCode}";'
        print("hello")
        query = SQL.sql_update(search_term)
        print(query)
        return query

    @staticmethod
    def deleteCoupon(couponCode):
        print("bye")
        search_term = f'DELETE FROM coupon WHERE coupon.CouponCode = "{couponCode}";'
        query = SQL.sql_update(search_term)
        print("hi")

        return query