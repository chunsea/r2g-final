import entity.SQL as SQL

class UserProfile:
    @staticmethod
    def addUserProf(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt):
        create_profile = f'INSERT INTO userprofile(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt)' \
                         f'VALUE ("{roles}","{m_userAcc}","{m_UserProf}","{m_MenuItem}","{m_MenuCat}","{m_Coupon}","{m_Orders}","{m_Rpt}");'
        
        query = SQL.sql_insert(create_profile)
        
        return query
    
    @staticmethod
    def displayUserProf():
        get_profile = f'SELECT * from userprofile'

        query = SQL.sql_retrieve_all(get_profile)

        return query
    
    @staticmethod
    def validateSearch(roles):
        get_profile = f'SELECT roles from userprofile where roles ="{roles}"'

        query = SQL.sql_retrieve_all(get_profile)

        return query
    
    @staticmethod
    def updateUserAcc(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt):
        set_fkcheck_false = f'SET FOREIGN_KEY_CHECKS = 0;'
        query = SQL.sql_update(set_fkcheck_false)
       
        search_term = f'UPDATE userprofile SET roles = "{roles}", m_userAcc = "{m_userAcc}", m_UserProf = "{m_UserProf}", m_MenuItem = "{m_MenuItem}", '\
                      f'm_MenuCat = "{m_MenuCat}", m_Coupon = "{m_Coupon}", m_Orders = "{m_Orders}", m_Rpt = "{m_Rpt}" ' \
                      f'WHERE roles = "{roles}";'
        
        query = SQL.sql_update(search_term)
        
        set_fkcheck_true = f'SET FOREIGN_KEY_CHECKS=1;'
        query = SQL.sql_update(set_fkcheck_true)
        
        return query
    
    @staticmethod
    def retrieveUserProfInfo():
        search_term = f'SELECT roles FROM userprofile ;'
        query = SQL.sql_retrieve_all(search_term)        
        return query
    
    @staticmethod
    def displayUserProf():
        get_profile = f'SELECT * FROM userprofile;'

        query = SQL.sql_retrieve_all(get_profile)

        return query
    
    @staticmethod
    def retrieveSearchUserProfInfo(roles):
        search_term = f'SELECT roles, m_userAcc, m_UserProf,m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt ' \
                      f'FROM userprofile ' \
                      f'WHERE roles = "{roles}";'
        query = SQL.sql_retrieve_all(search_term)

        return query