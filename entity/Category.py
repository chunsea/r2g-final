import entity.SQL as SQL


class Category:
    def __init__(self):
        pass

    @staticmethod
    def addCategory(category):
        add_category = f'INSERT INTO category (CategoryName) VALUES ("{category}")'
        print(add_category)
        query = SQL.sql_insert(query=add_category)
        print(query)

        return query

    @staticmethod
    def displayCategory() -> []:
        get_all_category = f'SELECT * FROM category'

        query = SQL.sql_retrieve_all(get_all_category)

        return query

    @staticmethod
    def validateSearch(category):
        search_term = f'SELECT CategoryName FROM category where CategoryName = "{category}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveCategoryInfo(category):
        search_term = f'SELECT * FROM category where CategoryName = "{category}"'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def retrieveSearchCategoryInfo(category):
        search_term = f'SELECT CategoryName FROM category where CategoryName = "{category}";'
        query = SQL.sql_retrieve_all(search_term)

        return query

    @staticmethod
    def updateCategory(category, newCategory):
        search_term = f'UPDATE category SET category.CategoryName = "{newCategory}" WHERE category.CategoryName = "{category}";'
        print(search_term)
        query = SQL.sql_update(search_term)
        print(query)
        return query

    @staticmethod
    def deleteCategory(category):
        search_term = f'DELETE FROM category WHERE category.CategoryName = "{category}";'
        query = SQL.sql_update(search_term)

        return query