CREATE DATABASE  IF NOT EXISTS `restaurant` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `restaurant`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: restaurant
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `CategoryName` varchar(45) NOT NULL,
  PRIMARY KEY (`CategoryName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('dessert'),('drink'),('main'),('sides');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coupon` (
  `CouponCode` varchar(45) NOT NULL,
  `DiscountPrice` int DEFAULT NULL,
  PRIMARY KEY (`CouponCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon`
--

LOCK TABLES `coupon` WRITE;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
INSERT INTO `coupon` VALUES ('10OFF',10),('20OFF',20),('25OFF',25),('APRIL10',10),('APRIL3',3),('APRIL5',5),('CASHBACK20',20),('CASHBACK30',30),('CASHBACK5',5),('FALL10',10),('FALL20',20),('FALL25',25),('LABORDAY15',15),('LABORDAY25',25),('LOVERSDAY',5),('MOTHER10',10),('SINGLESDAY',5);
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menuitem`
--

DROP TABLE IF EXISTS `menuitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menuitem` (
  `itemName` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`itemName`),
  KEY `FK1category_idx` (`category`),
  CONSTRAINT `FK1category` FOREIGN KEY (`category`) REFERENCES `category` (`CategoryName`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menuitem`
--

LOCK TABLES `menuitem` WRITE;
/*!40000 ALTER TABLE `menuitem` DISABLE KEYS */;
INSERT INTO `menuitem` VALUES ('BAKED CLAMS','Clam with Black Pepper','sides',3.90),('CARBONARA','Egg Sauce with Bacon','main',5.90),('CHEESE HAMBURGER','Beef Hamburger with Mixed Cheese','main',6.90),('CHEF SALAD','Parmesan Cheese, Crouton','sides',4.20),('CHICKEN SALAD','Sliced Tender Chicken','sides',4.90),('CHICKEN WING','8pcs','sides',7.90),('Coca Cola','Coca Cola','drink',1.80),('CORN CREAM SOUP','Creamy Soup made with Corn and Carrot','sides',2.90),('CRISPY CHICKEN','Crispy and flavourful Chicken','sides',3.90),('EGG SALAD','Egg, Mayonaisse','sides',4.90),('FANTA','FANTA','drink',1.80),('HAMBURGER','Beef Hamburger with Demi Sauce','main',5.90),('HAMBURGER & SAUSAGE','Hamburger with Pork Sausage','main',7.90),('ITALIAN WINE Red Wine/White Wine','BY GLASS','drink',3.90),('ITALY PASTA','ITALY PASTA with Agolio olio sauce','main',5.90),('LAKSA SEAFOOD','Laksa Sauce with Shrimp and Mussel','main',7.90),('MILO','MILO','drink',1.80),('MUSHROOM SOUP','Creamy Soup made with Button Mushroom','sides',2.90),('NACHO BACON POTATO','Bacon and Potato topped with Nacho Cheese','sides',3.90),('OVEN GRILLED ESCARGOTS','Escargots in Garlic Butter & Sautéed Onion','sides',5.90),('PUMPKIN SOUP','Creamy Soup made with Pumpkin','sides',3.20),('SAUTÉED BROCCOLI','Sautéed Broccoli\nand Mushroom with Garlic','sides',2.90),('SIRLOIN STEAK','Beef Steak with Black Pepper Sauce','main',11.90),('SMOKED SALMON','SMOKED SALMON','sides',3.90),('Sprint','Sprint','drink',1.80),('STEAMED MUSSELS','Steamed Mussels with Soup and Olive Oil','sides',4.90),('VONGOLE SPICY TOMATO','Spicy Tomato Sauce with Clam','main',5.90);
/*!40000 ALTER TABLE `menuitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `tableNum` int NOT NULL,
  `foodname` varchar(45) DEFAULT NULL,
  `item_desc` varchar(45) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `NumOfSet` int DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `total_price` int DEFAULT NULL,
  `PhoneNum` int DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tableNum`),
  KEY `FK1foodname_idx` (`foodname`),
  KEY `FK2PhoneNum_idx` (`PhoneNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'BAKED CLAMS','Clam with Black Pepper',3.90,1,1,4,NULL,NULL);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `report` (
  `PhoneNum` int NOT NULL,
  `date` varchar(45) DEFAULT NULL,
  `week` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PhoneNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (81217111,'12/03/2022','2');
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useraccount`
--

DROP TABLE IF EXISTS `useraccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `useraccount` (
  `fullname` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `nric_fin` varchar(45) DEFAULT NULL,
  `bday` varchar(45) DEFAULT NULL,
  `employeeid` int NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `roles` varchar(45) DEFAULT NULL,
  `is_active` tinyint DEFAULT NULL,
  PRIMARY KEY (`employeeid`),
  KEY `FK1roles` (`roles`),
  CONSTRAINT `FK1roles` FOREIGN KEY (`roles`) REFERENCES `userprofile` (`role`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccount`
--

LOCK TABLES `useraccount` WRITE;
/*!40000 ALTER TABLE `useraccount` DISABLE KEYS */;
INSERT INTO `useraccount` VALUES ('CHUN HWEE','WOODLANDS','S1234567D','1995-03-19',6745611,'password','owner',1);
/*!40000 ALTER TABLE `useraccount` ENABLE KEYS */;
INSERT INTO `useraccount` VALUES ('CHUN HWEE','WOODLANDS','S1234567D','1995-03-19',1234567,'any','manager',1);
/*!40000 ALTER TABLE `useraccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useradmin`
--

DROP TABLE IF EXISTS `useradmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `useradmin` (
  `employeeid` int NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`employeeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useradmin`
--

LOCK TABLES `useradmin` WRITE;
/*!40000 ALTER TABLE `useradmin` DISABLE KEYS */;
INSERT INTO `useradmin` VALUES (6745611,'password');
/*!40000 ALTER TABLE `useradmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userprofile`
--

DROP TABLE IF EXISTS `userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userprofile` (
  `role` varchar(45) NOT NULL,
  `m_useracc` tinyint DEFAULT NULL,
  `m_UserProf` tinyint DEFAULT NULL,
  `m_MenuItem` tinyint DEFAULT NULL,
  `m_MenuCat` tinyint DEFAULT NULL,
  `m_Coupon` tinyint DEFAULT NULL,
  `m_Orders` tinyint DEFAULT NULL,
  `m_Rpt` tinyint DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userprofile`
--

LOCK TABLES `userprofile` WRITE;
/*!40000 ALTER TABLE `userprofile` DISABLE KEYS */;
INSERT INTO `userprofile` VALUES ('manager',0,0,1,1,1,0,0),('owner',0,0,0,0,0,0,1),('staff',0,0,0,0,0,1,0),('useradmin',1,1,0,0,0,0,0);
/*!40000 ALTER TABLE `userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ItemAvailability`
--
-- DROP TABLE IF EXISTS `ItemAvailability`;
-- /*!40101 SET @saved_cs_client     = @@character_set_client */;
-- /*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ItemAvailability` (
  itemName varchar(45) NOT NULL,
  is_avail tinyint DEFAULT NULL,
  PRIMARY KEY (itemName),
  KEY FK1menuitem_idx (itemName),
  CONSTRAINT FK1menuitem FOREIGN KEY (itemName) REFERENCES menuitem (itemName) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'restaurant'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-25 16:27:00
