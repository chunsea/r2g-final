from entity.Coupon import Coupon

class RestManagerCreateCouponController:
    @staticmethod
    def createCoupon(couponCode, discount):
        if not all([couponCode, discount]):
            return False
        return Coupon.createCoupon(couponCode, discount)