from entity.MenuItem import MenuItem

class RestManagerDeleteMenuItemController:
    @staticmethod
    def deleteMenuItem(itemName):
        return MenuItem.deleteMenuItem(itemName)