from entity.UserAccount import UserAccount

class AdminSearchUserAccController:
    @staticmethod
    def displayUserAccInfo(employeeID):
        return UserAccount.validateSearch(employeeID)