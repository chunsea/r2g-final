from entity.UserAccount import UserAccount


class CreateAccountController:

    def __init__(self):
        self.account = UserAccount()

    @staticmethod
    def validateSearch(employeeId):

        return True if employeeId else False

    def createUserAcc(self, full_name, address, nric, birthday, EmployeeID, password, role, is_active):
        if not all([full_name, address, nric, birthday, EmployeeID, password, role, is_active]):
            return False
        return self.account.addUserAcc(full_name, address, nric, birthday, EmployeeID, password, role, is_active)

    def displayUserAcc(self):
        return self.account.displayUserAcc()

    def displayUserAccInfo(self, employeeID):
        if self.validateSearch(employeeID):
            return self.account.validateSearch(employeeID)

        return None

    def editUserAcc(self, EmployeeID):
        return self.account.updateUserAcc(EmployeeID)

    def displayUserData(self, EmployeeID):
        return self.account.retrieveUserAccInfo(EmployeeID)
