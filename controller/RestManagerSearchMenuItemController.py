from entity.MenuItem import MenuItem

class RestManagerSearchMenuItemController:
    @staticmethod
    def displayMenuItemInfo(itemName):
        return MenuItem.validateSearch(itemName)