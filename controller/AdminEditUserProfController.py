from entity.UserProfile import UserProfile

class AdminEditUserProfController:
    @staticmethod
    def editUserProf(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt):
        return UserProfile.updateUserAcc(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt)
    
    @staticmethod
    def retrieveUserProfInfo(roles):
        return UserProfile.retrieveUserProfInfo(roles)
    
    @staticmethod
    def displayUserProf():
        return UserProfile.displayUserProf()
    
    @staticmethod
    def retrieveSearchUserProfInfo(roles):
        return UserProfile.retrieveSearchUserProfInfo(roles)