from entity.MenuItem import MenuItem

class RestManagerCreateMenuItemController:
    @staticmethod
    def addMenuItem(itemName, itemDescription, category, price):
        if not all([itemName, itemDescription, category, price]):
            return False
        return MenuItem.addMenuItem(itemName, itemDescription, category, price)