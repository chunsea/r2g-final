from entity.Category import Category


class RestManagerEditCategoryController:
    def __init__(self):
        pass

    @staticmethod
    def editCategory(CategoryName, newCategoryname):
        return Category.updateCategory(CategoryName, newCategoryname)

    @staticmethod
    def retrieveCategoryInfo(CategoryName):
        return Category.retrieveCategoryInfo(CategoryName)

    @staticmethod
    def retrieveSearchCategoryInfo(CategoryName):
        return Category.retrieveCategoryInfo(CategoryName)