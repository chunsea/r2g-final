from entity.UserAccount import UserAccount

class AdminEditUserAccController:
    @staticmethod
    def editUserAcc(full_name, address, nric, birthday, employeeID, password, role, is_active):
        return UserAccount.updateUserAcc(full_name, address, nric, birthday, employeeID, password, role, is_active)
    
    @staticmethod
    def retrieveUserAccInfo(employeeID):
        return UserAccount.retrieveUserAccInfo(employeeID)
    
    @staticmethod
    def displayUserAcc():
        return UserAccount.displayUserAcc()
    
    @staticmethod
    def retrieveUserProf(employeeID):
        return UserAccount.retrieveUserProf(employeeID)
    
    @staticmethod
    def retrieveSearchUserAccInfo(employeeID):
        return UserAccount.retrieveSearchUserAccInfo(employeeID)