from entity.MenuItem import MenuItem

class RestManagerEditMenuItemController:
    @staticmethod
    def updateMenuItem(itemName, itemDescription, category, price):
        return MenuItem.updateMenuItem(itemName, itemDescription, category, price)
    
    @staticmethod
    def retrieveMenuItemInfo(itemName):
        return MenuItem.retrieveMenuItemInfo(itemName)
    
    @staticmethod
    def displayMenuItem():
        return MenuItem.displayMenuItem()
    
    @staticmethod
    def retrieveCategory(itemName):
        return MenuItem.retrieveCategory(itemName)
    
    @staticmethod
    def retrieveSearchMenuItemInfo(itemName):
        return MenuItem.retrieveSearchMenuItemInfo(itemName)