# Controller
from entity.Category import Category

class RestManagerCreateCategoryController:
    def __init__():
        pass
    @staticmethod
    def addCategory(CategoryName):
        if not all([CategoryName]):
            return False
        return Category.addCategory(CategoryName)