from entity.Coupon import Coupon

class RestManagerSearchCouponController:
    @staticmethod
    def displayCouponInfo(couponCode):
        return Coupon.validateSearch(couponCode)