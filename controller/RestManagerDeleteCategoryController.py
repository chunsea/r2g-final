from entity.Category import Category


class RestManagerDeleteCategoryController:
    def __init__(self):
        pass
    @staticmethod
    def deleteCategory(CategoryName):
        return Category.deleteCategory(CategoryName)
