from entity.UserProfile import UserProfile


class UserProfileController:

    def __init__(self):
        self.userprofile = UserProfile()

    def retrieveUserProfInfo(self):
        return self.userprofile.retrieveUserProfInfo()

    def create_user_profile(self, roles, privileges):
        return self.userprofile.create_user_profile(roles, privileges)

    def createUserProf(self, role, manage_user_account,
                       manage_user_profile, manage_menu_items, manage_menu_cat, manage_coupon_codes,
                       manage_orders, manage_report
                       ):
        if role is None:
            return False
        return self.userprofile.addUserProf(role, manage_user_account,
                                            manage_user_profile, manage_menu_items, manage_menu_cat,
                                            manage_coupon_codes,
                                            manage_orders, manage_report)

    def onSearch(self, role):
        return True

    def displayUserProfInfo(self, role):
        return True
