import entity.SQL as SQL


class UserProfile:

    def __init__(self):
        pass

    def retrieveUserProfInfo(self):
        get_all_profile = f'SELECT roles FROM userprofile'

        query = SQL.sql_retrieve_all(get_all_profile)

        return query

    def addUserProf(self, role, manage_user_account,
                    manage_user_profile, manage_menu_items, manage_menu_cat, manage_coupon_codes,
                    manage_orders, manage_report):
        create_profile = f'INSERT INTO userprofile VALUES (default, "{role}", {manage_user_account} ,' \
                         f'{manage_user_profile}, {manage_menu_items}, {manage_menu_cat},' \
                         f'{manage_coupon_codes}, {manage_orders} , {manage_report})'

        query = SQL.sql_insert(create_profile)

        return query

    def validateSearch(self, role):
        get_profile = f'SELECT roles from userprofile where roles ="{role}"'

        query = SQL.sql_retrieve_all(get_profile)

        return query

    def displayUserProf(self, role):
        get_profile = f'SELECT roles from userprofile where roles ="{role}"'

        query = SQL.sql_retrieve_all(get_profile)

        return query

# update query

# def updateUserProf(self,role):

# 	update_prof = f''
