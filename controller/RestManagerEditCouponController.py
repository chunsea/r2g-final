from entity.Coupon import Coupon

class RestManagerEditCouponController:
    @staticmethod
    def editCoupon(couponCode, discount):
        return Coupon.updateCoupon(couponCode, discount)
    
    @staticmethod
    def retrieveCouponInfo(couponCode):
        return Coupon.retrieveCouponInfo(couponCode)
    
    @staticmethod
    def displayCoupon():
        return Coupon.displayCoupon()
    
    @staticmethod
    def retrieveSearchCouponInfo(couponCode):
        return Coupon.retrieveSearchCouponInfo(couponCode)