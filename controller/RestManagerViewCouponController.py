from entity.Coupon import Coupon

class RestManagerViewCouponController:
    @staticmethod
    def displayCoupon():
        return Coupon.displayCoupon()