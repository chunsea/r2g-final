from entity.UserProfile import UserProfile

class AdminCreateUserProfController:
    @staticmethod
    def addUserProf(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt):
        return UserProfile.addUserProf(roles, m_userAcc, m_UserProf, m_MenuItem, m_MenuCat, m_Coupon, m_Orders, m_Rpt)