from entity.Coupon import Coupon

class RestManagerDeleteCouponController:
    @staticmethod
    def deleteCoupon(couponCode):
        return Coupon.deleteCoupon(couponCode)