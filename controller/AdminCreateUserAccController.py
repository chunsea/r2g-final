from entity.UserAccount import UserAccount

class AdminCreateUserAccController:

    @staticmethod    
    def addUserAcc(full_name, address, nric, birthday, EmployeeID, password, role):
        if not all([full_name, address, nric, birthday, EmployeeID, password, role]):
            return False
        return UserAccount.addUserAcc(full_name, address, nric, birthday, EmployeeID, password, role)