import unittest
from main import app
from entity import SQL
from flask import Flask , session , url_for , request
import json

app.testing = True


class TestLogin(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        

    def tearDown(self):
        pass

    def test_home_page(self):
        '''
            check to see if homepage -> / and login_page -> /login can rendered
        '''
        response = self.app.get('/')

        self.assertTrue(response.status_code , 200)

    # def test_login_page(self):

    #     response = self.app.get('/login')

    #     self.assertTrue(response.status_code, 200)

    #-> 200 , 302 , 400 , 500

    #user story 1
    # def test_user_story_1(self):

    #     '''
    #     As a user admin, I want to be able to log in to an account 
    #     with my employeeID and password so that I can access and manage user accounts.
    #     '''
        
    #     response = self.app.post('/login', data ={'username': '6745611', \
    #         'password': 'password'})

    #     assert response.status_code == 302

    # #user story 2
    # def test_user_story_2(self):

    #     '''
    #     As a user admin, I want to be able to create user accounts with user information including full name, address, NRIC/FIN, birthday, EmployeeID, passwords and user profile (role types) 
    #     so that all users would have their individual account.
    #     '''

    #     response = self.app.post('/login', data ={'username': '6745611', \
    #         'password': 'password'})

    #     assert response.status_code  == 302



    #     response = self.app.post('/accounts/account_create',data = {'FULL NAME':'Jason Tan', \
    #         'ADDRESS':'BLK233, BUKIT TIMAH ROAD, SINGAPORE', 'NRIC_FIN':'S7822777', \
    #         'BIRTHDAY':'1/2/2022', 'EMPLOYEEID':'268', 'PASSWORD':'268', 'ROLES':'staff'})


    #     assert response.status_code == 200

    # #user story 3 
    # def test_user_story_3(self):

    #     '''
    #     As a user admin, I want to be able to edit the user information so 
    #     that I can keep the account up to date.
    #     '''
    #     response = self.app.post('/account/1/edit_account',data = {'FULL NAME':'Jason Tan', \
    #         'ADDRESS':'BLK233, BUKIT TIMAH ROAD, SINGAPORE', 'NRIC_FIN':'S7822777', \
    #         'BIRTHDAY':'1/2/2022', 'EMPLOYEEID':'1', 'PASSWORD':'268', 'ROLES':'staff'})

    #     assert response.status_code == 302

    # #user story 4 
    # def test_user_story_4(self):


    #     response = self.app.post('/account/1/edit_account',data = {'FULL NAME':'Jason Tan', \
    #         'ADDRESS':'BLK233, BUKIT TIMAH ROAD, SINGAPORE', 'NRIC_FIN':'S7822777', \
    #         'BIRTHDAY':'1/2/2022', 'EMPLOYEEID':'1', 'PASSWORD':'268', 'ROLES':'staff', 'r1': 0})

    #     assert response.status_code == 302

    # #user story 5
    # def test_user_story_5(self):

    #     '''
    #     As a user admin, I want to be able to search for a user account 
    #     using their EmployeeID so that I can edit the account easily.
    #     '''

    #     response = self.app.get('/account/view_account' , data= {'mySearch': 1})

    #     assert response.status_code == 200

    # #user story 6 
    # def test_user_story_6(self):

    #     '''
    #     As a user admin, I want to be able to view user accounts 
    #     so that I can see the list of all the accounts.
    #     '''
    #     response = self.app.get('/accounts/view_profile')

    #     assert response.status_code == 200

    # #user story 7 
    # def test_user_story_7(self):

    #     '''
    #     As a user admin, I want to be able to create a user profile 
    #     so that the new role and privileges would be uploaded to the system.
    #     '''
    #     response = self.app.post('/accounts/create_profile',data = {'roles': 'useradmin', 'p1': 1, 'p2': 1, 'p3': 0, 'p4': 0, 'p5': 0, 'p6': 0, 'p7': 0})

    #     assert response.status_code == 200

    # #user story 8
    # def test_user_story_8(self):
    #     '''
    #     As a user admin, I want to be able to edit a user 
    #     profile so that I can keep the user's role and privileges up-to-date.
    #     '''
    #     response = self.app.post('/accounts/admin/edit_profile', data = {'roles': 'useradmin', 'p1': 1, 'p2': 1, 'p3': 0, 'p4': 0, 'p5': 0, 'p6': 0, 'p7': 0})

    #     assert response.status_code == 200


    # #user story 9
    # def test_user_story_9(self):
    #     '''
    #     As a user admin, I want to be able to change the status (activate/suspend) 
    #     of user profiles so that I do not need to keep an unnecessary user profile.
    #     '''
    #     response = self.app.post('/accounts/admin/edit_profile', data = {'roles': 'useradmin', 'p1': 1, 'p2': 1, 'p3': 0, 'p4': 0, 'p5': 0, 'p6': 0, 'p7': 0})

    #     assert response.status_code == 200

    # #user story 10
    # def test_user_story_10(self):
    #     '''
    #     As a user admin, I want to be able to search for a user profile using a role name 
    #     so that I can edit the role and privileges easily.
    #     '''
    #     response = self.app.get('/accounts/view_profile', data={'mySearch': 'useradmin'})

    #     assert response.status_code == 200

    # #user story 11
    # def test_user_story_11(self):
    #     '''
    #     As a user admin, I want to be able to view user profiles 
    #     so that I can see the list of all the user profiles.
    #     '''
    #     response = self.app.get('/accounts/view_profile')

    #     assert response.status_code == 200

    # #user story 12
    # def test_user_story_12(self):
    #     '''
    #     As a user admin, I want to be able to log out from an 
    #     account so that my account would not be accessed by unauthorized users.
    #     '''
    #     response = self.app.get('/logout')

    #     assert response.status_code == 302










# class TestUserAdmin(unittest.TestCase):

#     def setUp(self):
#         self.app = app.test_client()




if __name__ == '__main__':
    unittest.main()